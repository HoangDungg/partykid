﻿using System.Reflection;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PartyKid.Domain;

namespace PartyKid.Infrastructure;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddContextPool(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString(Assembly.GetExecutingAssembly().GetName().Name);
        services.AddDbContext<DbContext, PartyKidDbContext>(options =>
                    options.UseSqlServer(connectionString,
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        sqlOptions.CommandTimeout((int)TimeSpan.FromMinutes(2).TotalSeconds);
                        sqlOptions.EnableRetryOnFailure();
                    }));
        services
        .AddIdentity<ApplicationUser, IdentityRole<int>>(config =>
        {
            config.SignIn.RequireConfirmedEmail = false;
            config.Password.RequiredLength = 8;
        })
        .AddEntityFrameworkStores<PartyKidDbContext>()
        .AddDefaultTokenProviders();
        return services;
    }
}
