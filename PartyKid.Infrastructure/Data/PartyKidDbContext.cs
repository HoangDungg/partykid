﻿using Entities.Coupon;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PartyKid.Domain;

namespace PartyKid.Infrastructure;

public class PartyKidDbContext : IdentityDbContext<ApplicationUser, IdentityRole<int>, int>
{
    public PartyKidDbContext(DbContextOptions<PartyKidDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyConfigurationsFromAssembly(typeof(PartyKidDbContext).Assembly);
    }

    public override int SaveChanges()
    {
        OnBeforeSaving();
        return base.SaveChanges();
    }

    #region Entities
    public DbSet<CouponType> CouponTypes { get; set; }
    #endregion

    #region SUPPORT FUNC
    private void OnBeforeSaving()
    {
        IEnumerable<EntityEntry> entries = ChangeTracker.Entries();
        foreach (EntityEntry entry in entries)
        {
            if (entry.Entity is BaseEntity trackableEntity)
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        trackableEntity.CreatedDate = DateTime.Now;
                        break;
                    case EntityState.Modified:
                        trackableEntity.UpdatedDate = DateTime.Now;
                        break;
                }
            }
        }
    }
    #endregion
}
