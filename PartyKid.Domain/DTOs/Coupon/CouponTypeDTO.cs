﻿namespace PartyKid.Domain;

public class CouponTypeDTO
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public decimal DiscountAmount { get; set; }
}
