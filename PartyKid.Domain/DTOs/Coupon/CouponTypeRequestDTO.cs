﻿namespace PartyKid.Domain;

public class CouponTypeRequestDTO
{
    public int Id { get; set; }
    public string Name { get; set; }
}
