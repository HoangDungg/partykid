﻿namespace PartyKid.Domain;

public class AddCouponType
{
    public string Name { get; set; }
    public string Description { get; set; }
}
