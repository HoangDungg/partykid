﻿namespace PartyKid.Domain;

public class LoginResponseDTO
{
    public LoginResponseDTO(UserDTO user, string token)
    {
        User = user;
        Token = token;
    }
    public UserDTO User { get; set; }
    public string Token { get; set; }
}
