using PartyKid.Domain;

namespace Entities.Coupon
{
    public class CouponType : BaseEntity
    {
        public string Name { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal ConditionAmount { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public DateTime? AvailableDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
    }
}