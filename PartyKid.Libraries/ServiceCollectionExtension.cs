﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using PartyKid.Application;
using PartyKid.Domain;

namespace PartyKid.Libraries;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddEntityFrameworkRepositories(this IServiceCollection services)
    {
        services.AddScoped(typeof(IQueryRepository<>), typeof(QueryRepository<>));
        services.AddScoped(typeof(ICommandRepository<>), typeof(CommandRepository<>));
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped(typeof(IBaseService<>), typeof(BaseService<>));
        return services;
    }

    public static IMvcBuilder AddWebApiCore(this IServiceCollection services)
    {
        IMvcBuilder mvcBuilder = services.AddRouting(x => x.LowercaseUrls = true)
                                         .AddControllers()
                                         .AddJsonOptions(x =>
                                         {
                                             x.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                                         });
        services.AddResponseCaching()
                .AddHttpContextAccessor();

        return mvcBuilder;
    }

    public static IServiceCollection AddApiVersioningCore(this IServiceCollection services)
    {
        services.AddApiVersioning(opts =>
            {
                opts.DefaultApiVersion = new ApiVersion(1, 0);
                opts.AssumeDefaultVersionWhenUnspecified = true;
            });
        return services;
    }

    public static IServiceCollection AddCookie(this IServiceCollection services)
    {
        services.ConfigureApplicationCookie(o =>
        {
            o.ExpireTimeSpan = TimeSpan.FromDays(5);
            o.SlidingExpiration = true;
        });

        services.Configure<DataProtectionTokenProviderOptions>(o =>
        {
            o.TokenLifespan = TimeSpan.FromHours(3);
        });
        return services;
    }

    public static IServiceCollection AddJwtConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
        IConfigurationSection appSettingsSection = configuration.GetSection("AppSettings");
        services.Configure<AppSettings>(appSettingsSection);

        AppSettings appSettings = appSettingsSection.Get<AppSettings>();
        byte[] key = Encoding.ASCII.GetBytes(appSettings.Secret);
        services.AddAuthentication(x =>
        {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = async context =>
                    {
                        IUserService userServices = context.HttpContext.RequestServices.GetService<IUserService>();
                        ApplicationUser user = await userServices.GetById(context.Principal.Identity.Name);
                        if (user == null)
                        {
                            context.Fail(new Exception("Unauthorized"));
                        }
                        return;
                    }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

        services.AddAuthorization(options =>
    {
        options.AddPolicy(nameof(RoleCollection.Admin), policy => policy.RequireRole(nameof(RoleCollection.Admin)));
        //options.AddPolicy(Roles.user, policy => policy.RequireRole(Roles.user));
        options.AddPolicy(nameof(RoleCollection.User), policy => policy.RequireAssertion(context =>
                                                                    context.User.HasClaim(c => c.Type == ClaimTypes.Role &&
                                                                                               (c.Value == nameof(RoleCollection.Admin) || c.Value == nameof(RoleCollection.User)))));
    });
        return services;
    }
}
