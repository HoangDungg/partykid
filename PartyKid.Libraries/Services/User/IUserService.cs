﻿using PartyKid.Domain;

namespace PartyKid.Libraries;

public interface IUserService
{
    Task<ApplicationUser> GetById(string id);
}
