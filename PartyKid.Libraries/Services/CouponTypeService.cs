﻿using Entities.Coupon;
using PartyKid.Domain;

namespace PartyKid.Libraries;

public class CouponTypeService : BaseService<CouponType>
{
    public CouponTypeService(IQueryRepository<CouponType> queryRepository, ICommandRepository<CouponType> commandRepository, UnitOfWork unitOfWork) : base(queryRepository, commandRepository, unitOfWork)
    {

    }
}
