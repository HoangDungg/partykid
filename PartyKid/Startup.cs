﻿using PartyKid.Application;
using PartyKid.Infrastructure;
using PartyKid.Libraries;

namespace PartyKid;

public class Startup
{
    private readonly IConfiguration configuration;
    private readonly IWebHostEnvironment environment;

    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
        this.configuration = configuration;
        environment = env;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddWebApiCore();
        services
        .AddJwtConfiguration(configuration);
        services.AddApiVersioningCore();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        services.AddContextPool(configuration);
        services.AddEntityFrameworkRepositories();
        services.AddAutoMapper(typeof(ConfigMapper));
        services.AddScoped<IUserService, UserService>();

        var provider = services.BuildServiceProvider().GetService<IServiceScopeFactory>().CreateScope().ServiceProvider;
        services.AddCoreDependencies(provider);
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseMiddleware<ExceptionHandleMiddleware>()
        .UseHttpsRedirection()
        .UseResponseCaching()
        .UseMiddleware<TokenHandleMiddleware>()
        .UseRouting()
        .UseAuthentication()
        .UseAuthorization()
        .UseEndpoints(enpoints =>
        {
            enpoints.MapControllers();
        });
    }
}
