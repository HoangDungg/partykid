﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Entities.Coupon;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PartyKid.Application;
using PartyKid.Domain;
using PartyKid.Libraries;

namespace PartyKid;


[Route("api/[controller]")]
public class CouponsController : BaseApi
{
    private readonly IQueryRepository<CouponType> _queryRepository;
    private readonly ICommandRepository<CouponType> _commandRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly AutoMapper.IConfigurationProvider _config;

    public CouponsController(IQueryRepository<CouponType> queryRepository, ICommandRepository<CouponType> commandRepository, IMapper mapper, AutoMapper.IConfigurationProvider config, IUnitOfWork unitOfWork)
    {
        _queryRepository = queryRepository;
        _commandRepository = commandRepository;
        _mapper = mapper;
        _config = config;
        _unitOfWork = unitOfWork;
    }

    [Authorize]
    [HttpGet]
    public async Task<IResponse> GetCouponTypes()
    {
        return Success<IList<CouponTypeDTO>>(data: await _queryRepository.InitQuery().ProjectTo<CouponTypeDTO>(_config).ToListAsync());
    }

    [HttpPost]
    public async Task<IResponse> CreateGame([FromBody] AddCouponType request)
    {
        _commandRepository.Add(entities: _mapper.Map<CouponType>(request));
        await _unitOfWork.SaveChangesAsync();
        return Success("Create Success");
    }
}
