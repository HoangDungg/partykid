namespace PartyKid;

public class Program
{

    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
    // public static void Main(string[] args)
    // {
    //     var builder = WebApplication.CreateBuilder(args);

    //     //Init Configuration
    //     var configurationBuilder = new ConfigurationBuilder()
    //     .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
    //     .AddJsonFile("appsettings.json")
    //     .AddEnvironmentVariables()
    //     .AddCommandLine(args);

    //     IServiceCollection services = builder.Services;
    //     IConfiguration configuration = configurationBuilder.Build();

    //     // Add services to the container.


    //     var app = builder.Build();
    //     if (app.Environment.IsDevelopment())
    //     {
    //         app.UseSwagger();
    //         app.UseSwaggerUI();
    //     }

    //     app.UseHttpsRedirection();
    //     app.UseAuthentication();

    //     app.MapControllers();
    //     app.Run();
    // }
}
