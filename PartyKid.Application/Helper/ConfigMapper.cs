﻿using AutoMapper;
using Entities.Coupon;
using PartyKid.Domain;

namespace PartyKid.Application;

public class ConfigMapper : Profile
{
    public ConfigMapper()
    {
        CouponTypeConfiguration();
        AuthConfiguration();
    }

    private void CouponTypeConfiguration()
    {
        CreateMap<CouponType, CouponTypeDTO>().ReverseMap();
        CreateMap<AddCouponType, CouponType>().ReverseMap();
    }

    private void AuthConfiguration()
    {
        CreateMap<RegisterRequest, ApplicationUser>()
        .ForMember(x => x.UserName, opt => opt.MapFrom(x => x.Email))
        .ForSourceMember(x => x.Password, opt => opt.DoNotValidate())
        .ForSourceMember(x => x.ConfirmPassword, opts => opts.DoNotValidate())
        .ReverseMap();
        CreateMap<UserDTO, ApplicationUser>().ReverseMap();
    }
}
