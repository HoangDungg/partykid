﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace PartyKid.Application;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddCoreDependencies(this IServiceCollection services, IServiceProvider provider)
    {
        #region Exception Handling

        if (provider.GetService<IExceptionHandler<FluentValidation.ValidationException, ValidationProblemDetails>>() is null)
        {
            services.AddScoped(typeof(IExceptionHandler<FluentValidation.ValidationException, ValidationProblemDetails>), typeof(ValidationExceptionHandler));
        }

        if (provider.GetService<IExceptionHandler<Exception, ProblemDetails>>() is null)
        {
            services.AddScoped(typeof(IExceptionHandler<Exception, ProblemDetails>), typeof(UnhandledExceptionHandler));
        }

        #endregion
        return services;
    }

    public static IServiceCollection AddDefaultExceptionHandlers(this IServiceCollection services)
    {
        services.AddScoped(typeof(IExceptionHandler<FluentValidation.ValidationException, ProblemDetails>), typeof(ValidationExceptionHandler));
        services.AddScoped(typeof(IExceptionHandler<Exception, ProblemDetails>), typeof(UnhandledExceptionHandler));
        return services;
    }
}
