﻿using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PartyKid.Domain;

namespace PartyKid.Application;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class AuthorizeAttribute : Attribute, IAuthorizationFilter
{
    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var user = context.HttpContext.Items["User"];
        if (user == null)
        {
            // not logged in
            context.Result = new JsonResult(new
            {
                Status = (int)HttpStatusCode.Unauthorized,
                Title = Constants.AuthHandling.Messages.UnAuthorized,
                message = Constants.AuthHandling.Messages.UnAuthorized
            })
            { StatusCode = StatusCodes.Status401Unauthorized };
        }
    }
}
