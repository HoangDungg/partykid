﻿using Microsoft.AspNetCore.Mvc;

namespace PartyKid.Application;

public interface IExceptionHandler<in TException, out TOuput> where TException : Exception where TOuput : ProblemDetails
{
    TOuput Handle(TException exception);
}
